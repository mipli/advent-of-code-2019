use crate::{parse, Exercise};
use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::str::FromStr;

pub struct Day03 {
    wires: Vec<Wire>,
}

impl Default for Day03 {
    fn default() -> Day03 {
        Day03 { wires: vec![] }
    }
}

impl Day03 {
    fn get_wires(&self, path: &Path) -> Result<Vec<Wire>, String> {
        parse(path)
            .map_err(|e| format!("{:?}", e))
            .unwrap()
            .try_fold(vec![], |mut wires, wire: Result<Wire, _>| {
                wires.push(wire?);
                Ok(wires)
            })
    }
}

impl Exercise for Day03 {
    fn part1(&mut self, path: &Path) {
        self.wires = self
            .get_wires(path)
            .expect("Could not extract wire information");

        let man_distance = self.wires[0]
            .intersections(&self.wires[1])
            .iter()
            .map(|point| point.distance())
            .min();
        println!("intersection distance: {:?}", man_distance);
    }

    fn part2(&mut self, _path: &Path) {
        let step_distance = self.wires[0]
            .intersections(&self.wires[1])
            .iter()
            .map(|point| self.wires[0].steps(point).unwrap() + self.wires[1].steps(point).unwrap())
            .min();

        println!("step distance: {:?}", step_distance);
    }
}

#[derive(Debug, Hash, Eq, PartialEq)]
struct Point(i32, i32);

impl Point {
    fn distance(&self) -> i32 {
        self.0.abs() + self.1.abs()
    }
}

#[derive(Debug)]
struct Wire {
    pub points: HashSet<Point>,
    pub steps: HashMap<Point, u32>,
}

impl Wire {
    fn steps(&self, point: &Point) -> Option<&u32> {
        self.steps.get(point)
    }

    fn intersections<'a>(&'a self, other: &'a Wire) -> Vec<&'a Point> {
        self.points.intersection(&other.points).collect()
    }
}

impl Default for Wire {
    fn default() -> Wire {
        Wire {
            points: HashSet::default(),
            steps: HashMap::default(),
        }
    }
}

impl FromStr for Wire {
    type Err = String;

    fn from_str(s: &str) -> Result<Wire, Self::Err> {
        let mut wire = Wire::default();
        let mut x = 0;
        let mut y = 0;
        let mut steps = 0;
        s.split(',').for_each(|path| {
            let path = path.trim();
            if path.len() < 2 {
                return;
            }
            let num = usize::from_str(&path[1..]).unwrap();
            let modifier: Box<dyn Fn(&mut i32, &mut i32)> = match path.chars().next() {
                Some('R') => Box::new(|x: &mut i32, _: &mut i32| {
                    *x += 1;
                }),
                Some('L') => Box::new(|x: &mut i32, _: &mut i32| {
                    *x -= 1;
                }),
                Some('U') => Box::new(|_: &mut i32, y: &mut i32| {
                    *y += 1;
                }),
                Some('D') => Box::new(|_: &mut i32, y: &mut i32| {
                    *y -= 1;
                }),
                _ => {
                    panic!("Invalid direction");
                }
            };
            for _ in 0..num {
                modifier(&mut x, &mut y);
                steps += 1;
                wire.points.insert(Point(x, y));
                wire.steps.insert(Point(x, y), steps);
            }
        });
        Ok(wire)
    }
}
