use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::str::FromStr;

mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;
mod day07;
mod day08;
mod day09;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod intcode;
pub use intcode::{Intcode, State};

pub trait Exercise {
    fn part1(&mut self, path: &Path);
    fn part2(&mut self, path: &Path);
}

pub fn execute(day: u8, input: &Path, no_part1: bool) {
    if !input.exists() {
        println!("input file not found: {}", input.to_string_lossy());
        return;
    }
    let exercise: Option<Box<dyn Exercise>> = match day {
        1 => Some(Box::new(day01::Day01::default())),
        2 => Some(Box::new(day02::Day02::default())),
        3 => Some(Box::new(day03::Day03::default())),
        4 => Some(Box::new(day04::Day04::default())),
        5 => Some(Box::new(day05::Day05::default())),
        6 => Some(Box::new(day06::Day06::default())),
        7 => Some(Box::new(day07::Day07::default())),
        8 => Some(Box::new(day08::Day08::default())),
        9 => Some(Box::new(day09::Day09::default())),
        10 => Some(Box::new(day10::Day10::default())),
        11 => Some(Box::new(day11::Day11::default())),
        12 => Some(Box::new(day12::Day12::default())),
        13 => Some(Box::new(day13::Day13::default())),
        14 => Some(Box::new(day14::Day14::default())),
        _ => None,
    };
    match exercise {
        None => println!("exercise {} is not available", day),
        Some(mut exercise) => {
            if !no_part1 {
                exercise.part1(input);
            }
            exercise.part2(input);
        }
    }
}

enum ParseError {
    Empty,
    Other(String),
}

pub fn parse<T>(path: &Path) -> std::io::Result<impl Iterator<Item = Result<T, String>>>
where
    T: FromStr,
{
    let file = File::open(path)?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    Ok(std::iter::from_fn(move || {
        buf.clear();
        match reader
            .read_line(&mut buf)
            .map_err(|_| ParseError::Empty)
            .and_then(|_| {
                if buf.is_empty() {
                    Err(ParseError::Empty)
                } else {
                    T::from_str(buf.trim()).map_err(|_| ParseError::Other(buf.to_string()))
                }
            }) {
            Ok(j) => Some(Ok(j)),
            Err(ParseError::Other(s)) => Some(Err(format!("Could not parse: {}", s.trim()))),
            Err(ParseError::Empty) => None,
        }
    })
    .fuse())
}
