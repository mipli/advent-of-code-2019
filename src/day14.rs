use nom::{
  IResult,
  bytes::complete::{tag, take_while},
  combinator::map_res,
  multi::separated_nonempty_list,
};

use std::fs;

use crate::{Exercise};
use std::path::Path;
use std::str::FromStr;
use std::collections::{HashMap, HashSet};

pub struct Day14 { }

impl Day14 {
}

impl Default for Day14 {
    fn default() -> Day14 {
        Day14 {
        }
    }
}

impl Exercise for Day14 {
    fn part1(&mut self, path: &Path) {
        let content = fs::read_to_string(path).expect("Could not read input");
        let mut machine = Machine::default();
        let reactions = content.lines().map(|line| {
            let (_, reaction) = parse_line(line).expect("Could not parse input");
            reaction
        }).collect::<Vec<_>>();
        machine.add_reactions(reactions);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        println!("Output: {:?}", output);
    }

    fn part2(&mut self, path: &Path) {
        let content = fs::read_to_string(path).expect("Could not read input");
        let mut base = Machine::default();
        let reactions = content.lines().map(|line| {
            let (_, reaction) = parse_line(line).expect("Could not parse input");
            reaction
        }).collect::<Vec<_>>();
        base.add_reactions(reactions);

        let limit: i64 = 1_000_000_000_000;
        let (output, _) = base.get_fuel_requirement(1).expect("Should find fuel requirement");

        let mut output: i64 = output as i64;

        let mut lower  = limit / output;
        let mut upper  = lower * 2;
        let mut attempt = (lower + upper) / 2;
        loop {
            let machine = base.clone();
            let o = machine.get_fuel_requirement(attempt as i64).expect("Should find fuel requirement");
            output = o.0 as i64;
            let n = if output < limit {
                lower = attempt;
                (attempt + upper) / 2
            } else if output > limit {
                upper = attempt;
                (attempt + lower) / 2
            } else {
                attempt
            };
            if n == attempt {
                break;
            }
            attempt = n;
        }
        println!("Output: {:?}", attempt);
    }
}

pub type Component = (i64, String);

#[derive(Clone)]
pub struct Machine {
    reactions: HashMap<Component, Vec<Component>>,
}

impl Machine {
    pub fn add_reactions(&mut self, reactions: Vec<(Component, Vec<Component>)>) {
        reactions.iter().for_each(|(output, input)| {
            self.reactions.insert(output.clone(), input.clone());
        })
    }

    pub fn get_fuel_requirement(&self, goal: i64) -> Option<Component> {
        let mut requirements: HashMap<String, i64> = HashMap::default();
        let mut surplus: HashMap<String, i64> = HashMap::default();
        let reaction = self.reactions.get(&(1, "FUEL".to_string()))?;
        reaction.iter().for_each(|(num, comp)| {
            *(requirements.entry(comp.clone()).or_default()) += goal * *num;
        });
        let mut reqs: Vec<_> = requirements.iter().map(|(k, v)| (*v, k.to_string())).collect();
        while reqs.len() > 1 || (reqs.len() == 1 && reqs[0].1 != "ORE") {
            reqs = self.get_requirements(reqs, &mut surplus)?;
        }
        reqs.pop()
    }

    pub fn get_requirements(&self, base: Vec<Component>, surplus: &mut HashMap<String, i64>) -> Option<Vec<Component>> {
        let mut requirements: HashMap<String, i64> = HashMap::default();
        for (num, comp) in &base {
            let surplus_value = surplus.entry(comp.to_string()).or_default();
            let (extra, out) = self.get_input_requirements(&(*num, comp.to_string()), surplus_value)?;
            if extra > 0 {
                *surplus_value += extra;
            }
            out.iter().for_each(|(n, c)| {
                *(requirements.entry(c.clone()).or_default()) += n;
            });
        }
        Some(requirements.iter().map(|(k, v)| (*v, k.to_string())).collect::<Vec<_>>())
    }

    pub fn get_input_requirements(&self, output: &Component, surplus: &mut i64) -> Option<(i64, Vec<Component>)> {
        if output.1 == "ORE" {
            return Some((0, vec![output.clone()]));
        }
        if output.0 == *surplus {
            *surplus = 0;
            return Some((0, vec![]));
        }
        let options: Vec<(&Component, &Vec<Component>)> = self.reactions.iter().filter(|((_, t), _)| t == &output.1).collect::<Vec<_>>();

        if let Some((_, v)) = options.iter().find(|(c, _)| c.0 == output.0) {
            return Some((0, v.to_vec()));
        }

        let mut options = options.iter().map(|((c, a), b)| {
            if c < &output.0 {
                if c + *surplus >= output.0 {
                    ((*c, a), b.to_vec())
                } else {
                    let mut i = output.0 / c;
                    while (c * i) + *surplus >= output.0 {
                        i -= 1;
                    }
                    if (c * i) + *surplus < output.0 {
                        i += 1;
                    }
                    let reactions = b.iter().map(|(c, v)| (c * i, v.to_string())).collect::<Vec<_>>();
                    ((c * i, a), reactions)
                }
            } else {
                ((*c, a), b.to_vec())
            }
        }).collect::<Vec<_>>();
        options.sort_by(|a, b| {
            let a_diff = (a.0).0 - output.0;
            let b_diff = (b.0).0 - output.0;
            b_diff.cmp(&a_diff)
        });
        let o = options.pop()?;
        if (o.0).0 >= output.0 {
            Some(((o.0).0 - output.0, o.1))
        } else {
            let diff = output.0 - (o.0).0;
            *surplus -= diff;
            Some((0, o.1))

        }
    }
}

impl Default for Machine {
    fn default() -> Machine {
        Machine {
            reactions: HashMap::default(),
        }
    }
}

pub fn parse_part(input: &str) -> IResult<&str, Component> {
    let (input, num) = map_res(take_while(is_char_digit), i64::from_str)(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, id) = take_while(is_char_alphabetic)(input)?;
    Ok((input, (num, id.to_string())))
}

fn is_char_digit(c: char) -> bool {
    c.is_digit(10)
}

fn is_char_alphabetic(c: char) -> bool {
    c.is_alphabetic()
}

pub fn parse_input(input: &str) -> IResult<&str, Vec<Component>> {
    let (input, parts) = separated_nonempty_list(tag(", "), parse_part)(input)?;
    Ok((input, parts))
}

pub fn parse_line(input: &str) -> IResult<&str, (Component, Vec<Component>)> {
    let (input, parts) = parse_input(input)?;
    let (input, _) = tag(" => ")(input)?;
    let (input, output) = parse_part(input)?;
    Ok((input, (output, parts)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_single() {
        let (_, output) = parse_part("157 ORE").expect("Could not parse input");
        assert_eq!(output.0, 157);
        assert_eq!(output.1, "ORE");

    }

    #[test]
    fn parse_multiple() {
        let (_, output) = parse_input("3 A, 2 B").expect("Could not parse input");
        assert_eq!(output.len(), 2);
        assert_eq!(output[0].0, 3);
        assert_eq!(output[0].1, "A");
        assert_eq!(output[1].0, 2);
        assert_eq!(output[1].1, "B");

    }

    #[test]
    fn parse_with_output() {
        let (_, output) = parse_line("3 A, 2 B => 4 C").expect("Could not parse input");
        assert_eq!((output.0).0, 4);
        assert_eq!((output.0).1, "C");
        assert_eq!(output.1[0].0, 3);
        assert_eq!(output.1[0].1, "A");
        assert_eq!(output.1[1].0, 2);
        assert_eq!(output.1[1].1, "B");

    }

    #[test]
    fn machine_produce_exact_match() {
        let (_, reaction) = parse_line("3 A, 2 B => 4 C").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![reaction]);
        let output = machine.get_input_requirements(&(4, "C".to_string()), &mut 0).expect("Should find reaction requirement");
        assert_eq!(output.1.len(), 2);
    }

    #[test]
    fn machine_produce_more() {
        let (_, reaction) = parse_line("3 A, 2 B => 8 C").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![reaction]);
        let output = machine.get_input_requirements(&(8, "C".to_string()), &mut 0).expect("Should find reaction requirement");
        assert_eq!(output.1.len(), 2);
    }

    #[test]
    fn machine_produce_less() {
        let (_, reaction) = parse_line("1 A => 2 C").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![reaction]);
        let output = machine.get_input_requirements(&(4, "C".to_string()), &mut 0).expect("Should find reaction requirement");
        assert_eq!(output.1.len(), 1);
        assert_eq!(output.1[0].0, 2);
        assert_eq!(output.1[0].1, "A");
    }

    #[test]
    fn machine_production_with_surplus() {
        let (_, reaction) = parse_line("1 A => 2 C").expect("Could not parse input");
        let mut machine = Machine::default();
        let mut extra = 2;
        machine.add_reactions(vec![reaction]);
        let output = machine.get_input_requirements(&(4, "C".to_string()), &mut extra).expect("Should find reaction requirement");
        assert_eq!(output.1.len(), 1);
        assert_eq!(output.1[0].0, 1);
        assert_eq!(output.1[0].1, "A");
    }

    #[test]
    fn machine_production_with_more_surplus() {
        let (_, reaction) = parse_line("1 A => 2 C").expect("Could not parse input");
        let mut machine = Machine::default();
        let mut extra = 2;
        machine.add_reactions(vec![reaction]);
        let output = machine.get_input_requirements(&(6, "C".to_string()), &mut extra).expect("Should find reaction requirement");
        assert_eq!(output.1.len(), 1);
        assert_eq!(output.1[0].0, 2);
        assert_eq!(output.1[0].1, "A");
    }

    #[test]
    fn machine_simpe_fuel_requirement() {
        let (_, a) = parse_line("1 ORE => 2 A").expect("Could not parse input");
        let (_, b) = parse_line("2 A => 1 FUEL").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![a, b]);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        assert_eq!(output.0, 1);
        assert_eq!(output.1, "ORE");
    }

    #[test]
    fn machine_slightly_more_complex_fuel_requirement() {
        let (_, a) = parse_line("1 ORE => 2 A").expect("Could not parse input");
        let (_, b) = parse_line("1 ORE => 1 B").expect("Could not parse input");
        let (_, c) = parse_line("2 A, 2 B => 1 FUEL").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![a, b, c]);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        assert_eq!(output.0, 3);
        assert_eq!(output.1, "ORE");
    }

    #[test]
    fn machine_complex_fuel_requirement() {
        let (_, a) = parse_line("1 ORE => 2 A").expect("Could not parse input");
        let (_, b) = parse_line("1 ORE => 1 B").expect("Could not parse input");
        let (_, c) = parse_line("1 A, 1 B => 2 C").expect("Could not parse input");
        let (_, d) = parse_line("2 A, 2 B, 1 C => 1 FUEL").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![a, b, c, d]);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        assert_eq!(output.0, 5);
        assert_eq!(output.1, "ORE");
    }

    #[test]
    fn machine_complex_fuel_requirement_with_surplus() {
        let (_, a) = parse_line("1 ORE => 3 A").expect("Could not parse input");
        let (_, b) = parse_line("1 ORE => 1 B").expect("Could not parse input");
        let (_, c) = parse_line("1 A, 1 B => 2 C").expect("Could not parse input");
        let (_, d) = parse_line("2 A, 2 B, 1 C => 1 FUEL").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![a, b, c, d]);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        assert_eq!(output.0, 4);
        assert_eq!(output.1, "ORE");
    }

    #[test]
    fn machine_complex_fuel_requirement_with_different_ores() {
        let (_, a) = parse_line("2 ORE => 3 A").expect("Could not parse input");
        let (_, b) = parse_line("5 ORE => 1 B").expect("Could not parse input");
        let (_, c) = parse_line("1 A, 1 B => 2 C").expect("Could not parse input");
        let (_, d) = parse_line("4 C => 2 E").expect("Could not parse input");
        let (_, e) = parse_line("2 A, 2 B, 1 E => 1 FUEL").expect("Could not parse input");
        let mut machine = Machine::default();
        machine.add_reactions(vec![a, b, c, d, e]);
        let output = machine.get_fuel_requirement(1).expect("Should find fuel requirement");
        assert_eq!(output.0, 24);
        assert_eq!(output.1, "ORE");
    }
}
