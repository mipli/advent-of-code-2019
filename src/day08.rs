use crate::{Exercise};
use std::path::Path;
use std::fs;
use std::str::FromStr;

pub struct Day08 {
    picture: Option<Picture>,
}

impl Default for Day08 {
    fn default() -> Day08 {
        Day08 {
            picture: None,
        }
    }
}

impl Day08 {
}

impl Exercise for Day08 {
    fn part1(&mut self, path: &Path) {
        let content = fs::read_to_string(path).expect("Could not read input");
        let picture = Picture::from_string(&content, 25, 6);

        let layer = picture.get_layer_by_digit(0).expect("No layers in picture");
        let one_count = layer.grid.iter().filter(|&i| i == &1).count();
        let two_count = layer.grid.iter().filter(|&i| i == &2).count();
        println!("Sum: {:?}", one_count * two_count);

        self.picture = Some(picture);
    }

    fn part2(&mut self, _path: &Path) {
        if let Some(ref picture) = self.picture {
            picture
                .compose_picture()
                .chunks(picture.width)
                .for_each(|chunk| {
                    let line = chunk
                        .iter()
                        .map(|c| match c {
                            0 => '.',
                            1 => '1',
                            _ => '?',
                        }).collect::<String>();
                    println!("{}", line);
                });
        }
    }
}

#[derive(Debug)]
struct Layer {
    grid: Vec<u8>,
    width: usize,
    height: usize,
}

impl Layer {
    fn new(width: usize, height: usize) -> Self {
        Layer {
            grid: vec![0; width * height],
            width,
            height,
        }
    }
}

#[derive(Debug)]
struct Picture {
    layers: Vec<Layer>,
    width: usize,
    height: usize,
}

impl Picture {
    fn from_string(s: &str, width: usize, height: usize) -> Self {
        let layers = s.trim()
            .chars()
            .collect::<Vec<_>>()
            .chunks(width * height)
            .map(|data| {
                let mut layer = Layer::new(width, height);
                layer.grid = data.iter().map(|d| {
                    u8::from_str(&d.to_string()).unwrap()
                }).collect::<Vec<_>>();
                layer
            }).collect::<Vec<_>>();

        Picture {
            layers,
            width,
            height,
        }
    }

    fn get_layer_by_digit(&self, digit: u8) -> Option<&Layer> {
        self.layers.iter().min_by(|a, b| {
            let a_count = a.grid.iter().filter(|&ai| ai == &digit).count();
            let b_count = b.grid.iter().filter(|&bi| bi == &digit).count();
            a_count.cmp(&b_count)
        })
    }

    fn compose_picture(&self) -> Vec<u8> {
        (0..self.width * self.height).map(|i| {
            self.layers.iter().fold(2, |colour, layer| {
                if colour != 2 {
                    colour
                } else {
                    layer.grid[i]
                }
            })
        }).collect::<Vec<_>>()
    }
}
