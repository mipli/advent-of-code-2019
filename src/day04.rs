use crate::Exercise;
use std::collections::HashMap;
use std::path::Path;

pub struct Day04 {}

impl Default for Day04 {
    fn default() -> Day04 {
        Day04 {}
    }
}

impl Exercise for Day04 {
    fn part1(&mut self, _path: &Path) {
        let valid = (156_218..=652_527)
            .map(to_digits)
            .filter(|d| is_valid(&d))
            .count();
        println!("Valid count: {}", valid);
    }

    fn part2(&mut self, _path: &Path) {
        let valid = (156_218..=652_527)
            .map(to_digits)
            .filter(|d| is_extra_valid(&d))
            .count();
        println!("Extra valid count: {}", valid);
    }
}

fn to_digits(mut num: i32) -> Vec<i32> {
    let mut v = vec![];
    while num > 0 {
        v.push(num % 10);
        num /= 10;
    }
    v.into_iter().rev().collect()
}

fn is_valid(digits: &[i32]) -> bool {
    is_increasing(digits) && has_double(digits)
}

fn is_extra_valid(digits: &[i32]) -> bool {
    is_increasing(digits) && has_exact_double(digits)
}

fn is_increasing(digits: &[i32]) -> bool {
    digits.windows(2).all(|win| win[0] <= win[1])
}

fn has_double(digits: &[i32]) -> bool {
    !digits.windows(2).all(|win| win[0] != win[1])
}

fn has_exact_double(digits: &[i32]) -> bool {
    let mut matches: HashMap<i32, usize> = HashMap::default();
    digits.iter().for_each(|digit| {
        *(matches.entry(*digit).or_default()) += 1;
    });
    matches.values().any(|&count| count == 2)
}
