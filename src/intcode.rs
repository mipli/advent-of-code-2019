use std::{
    convert::TryFrom,
    ops::{Index, IndexMut}, 
    str::FromStr
};

#[derive(Debug)]
pub enum Op {
    Add,
    Multiply,
    Halt,
    Save,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    BaseAdjust,
}

#[derive(Debug)]
enum ParameterMode {
    Position,
    Immediate,
    Relative,
}

struct Context<'a> {
    pointer: &'a mut usize,
    relative_base: &'a mut usize,
    offset: usize,
}

#[derive(Debug)]
pub struct Operation {
    op: Op,
    parameter_modes: Vec<ParameterMode>,
}

impl TryFrom<&i64> for Operation {
    type Error = String;

    fn try_from(value: &i64) -> Result<Self, Self::Error> {
        let mut num: i64 = *value;
        let mut digits = vec![];
        digits.push(num % 100);
        num /= 100;
        while num > 0 {
            digits.push(num % 10);
            num /= 10;
        }
        let op = match digits[0] {
            1 => Ok(Op::Add),
            2 => Ok(Op::Multiply),
            3 => Ok(Op::Save),
            4 => Ok(Op::Output),
            5 => Ok(Op::JumpIfTrue),
            6 => Ok(Op::JumpIfFalse),
            7 => Ok(Op::LessThan),
            8 => Ok(Op::Equals),
            9 => Ok(Op::BaseAdjust),
            99 => Ok(Op::Halt),
            _ => Err(format!("Invalid Op code: {}", value).to_string())
        }?;
        let mut parameter_modes = vec![];
        for digit in &digits[1..] {
            match *digit {
                0 => parameter_modes.push(ParameterMode::Position),
                1 => parameter_modes.push(ParameterMode::Immediate),
                2 => parameter_modes.push(ParameterMode::Relative),
                _ => return Err("Invalid parameter mode".to_string())
            }
        }
        Ok(Operation {
            op,
            parameter_modes,
        })
    }
}

impl Operation {
    fn get_next_param(&self, context: &mut Context, memory: &mut Vec<i64>) -> i64 {
        let mode = self.parameter_modes.get(context.offset);
        let pointer = *context.pointer + context.offset;
        context.offset += 1;
        match mode {
            Some(ParameterMode::Immediate) => self.get_value(pointer + 1, memory),
            Some(ParameterMode::Relative) => self.get_value(((*context.relative_base as i64) + self.get_value(pointer + 1, memory)) as usize, memory),
            _ => self.get_value(self.get_value(pointer + 1, memory) as usize, memory),
        }
    }

    fn get_value(&self, pointer: usize, memory: &mut Vec<i64>) -> i64 {
        if pointer + 1 > memory.len() {
            0
        } else {
            memory[pointer]
        }
    }

    fn store_value(&self, value: i64, context: &mut Context, memory: &mut Vec<i64>) {
        let pointer = *context.pointer + context.offset;
        if pointer + 1 > memory.len() {
            memory.resize(pointer + 2, 0);
        }
        let pos = match self.parameter_modes.get(context.offset) {
            Some(ParameterMode::Immediate) => panic!("Cannot store immediate value!"),
            Some(ParameterMode::Relative) => (*context.relative_base as i64 + memory[pointer + 1]) as usize,
            _ => memory[pointer + 1] as usize,
        };
        if pos >= memory.len() {
            memory.resize(pos + 1, 0);
        }
        memory[pos] = value;
        context.offset += 1;
    }

    fn execute(&self, context: &mut Context, memory: &mut Vec<i64>, input: &mut Option<i64>) -> (State, Option<i64>) {
        match self.op {
            Op::Add => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                self.store_value(a + b, context, memory);
            },
            Op::Multiply => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                self.store_value(a * b, context, memory);
            },
            Op::Save => {
                if let Some(value) = input {
                    self.store_value(*value, context, memory);
                    *input = None;
                } else {
                    return (State::Waiting, None);
                }
            },
            Op::JumpIfTrue => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                if a != 0 {
                    *context.pointer = b as usize;
                    return (State::Running, None);
                }
            },
            Op::JumpIfFalse => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                if a == 0 {
                    *context.pointer = b as usize;
                    return (State::Running, None);
                }
            },
            Op::Output => {
                let output = self.get_next_param(context, memory);
                *context.pointer += context.offset + 1;
                return (State::Running, Some(output)); 
            },
            Op::LessThan => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                let value = if a < b {
                    1
                } else {
                    0
                };
                self.store_value(value, context, memory);
            },
            Op::Equals => {
                let a = self.get_next_param(context, memory);
                let b = self.get_next_param(context, memory);
                let value = if a == b {
                    1
                } else {
                    0
                };
                self.store_value(value, context, memory);
            },
            Op::Halt => { 
                return (State::Halt, None);
            },
            Op::BaseAdjust => {
                *context.relative_base = (*context.relative_base as i64 + self.get_next_param(context, memory)) as usize;
            }
        }
        *context.pointer += context.offset + 1;
        (State::Running, None)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum State {
    Waiting,
    Running,
    Errored,
    Halt,
}

#[derive(Debug, Clone)]
pub struct Intcode {
    memory: Vec<i64>,
    pub pointer: usize,
    pub state: State,
    input: Option<i64>,
    relative_base: usize,
}

impl Intcode {
    pub fn current_operation(&self) -> Result<Operation, String> {
        let num: &i64 = self.memory.get(self.pointer).ok_or_else(|| "Could not read memory".to_string())?;
        Operation::try_from(num)
    }

    pub fn step(&mut self) -> Result<Option<i64>, String> {
        let operation = self.current_operation()?;
        let mut context = Context {
            pointer: &mut self.pointer,
            relative_base: &mut self.relative_base,
            offset: 0,
        };
        let (result, output) = operation.execute(&mut context, &mut self.memory, &mut self.input);
        self.state = result;
        Ok(output)
    }

    pub fn input(&mut self, input: i64) {
        self.input = Some(input);
    }

    pub fn run_until_output(&mut self) -> Result<Option<i64>, String> {
        if self.state == State::Waiting {
            self.state = State::Running;
        }
        while self.state == State::Running {
            if let Ok(res) = self.step() {
                match res {
                    Some(_) => return Ok(res),
                    _ => {}
                }
            }
        }
        Ok(None)
    }

    pub fn run(&mut self, input: i64) -> Result<i64, String> {
        let mut buffer = (0, 0);
        self.input(input);
        if self.state == State::Waiting {
            self.state = State::Running;
        }
        while self.state == State::Running {
            let point = self.pointer;
            if let Some(res) = self.step().expect("Step failed") {
                if buffer.0 != 0 && self.state != State::Halt {
                    return Err(format!("Machine output failure {:?}@{:?}", buffer.0, point).to_string());
                }
                buffer = (res, point);
            }
        }
        Ok(buffer.0)
    }
}

impl FromStr for Intcode {
    type Err = String;

    fn from_str(s: &str) -> Result<Intcode, Self::Err> {
        let memory: Vec<i64> = s
            .split(',')
            .try_fold(vec![], |mut acc, c| { let num = i64::from_str(c.trim()).map_err(|_| "could not parse".to_string())?;
                acc.push(num);
                Ok(acc)
            })
            .map_err(|s: String| s)?;
        Ok(Intcode {
            memory,
            pointer: 0,
            relative_base: 0,
            state: State::Running,
            input: None,
        })
    }
}

impl Index<usize> for Intcode {
    type Output = i64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.memory[index]
    }
}

impl IndexMut<usize> for Intcode {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.memory[index]
    }
}


#[cfg(test)]
mod tests {
    use super::{Intcode, State};
    use std::str::FromStr;

    #[test]
    fn basic_operations() {
        let mut machine: Intcode = "1,1,1,4,99,5,6,0,99".parse().expect("Should be able to parse input");
        while machine.state == State::Running {
            assert!(machine.step().is_ok());
        }

        assert_eq!(machine.memory[0], 30);
    }

    #[test]
    fn can_read_position_parameters() {
        let mut machine: Intcode = "0001,1,1,4,99,5,6,0,99".parse().expect("Should be able to parse input");
        while machine.state == State::Running {
            assert!(machine.step().is_ok());
        }

        assert_eq!(machine.memory[0], 30);
    }

    #[test]
    fn can_read_immediate_paramters() {
        let mut machine: Intcode = "1102,1,2,5,1101,5,6,0,99".parse().expect("Should be able to parse input");
        while machine.state == State::Running {
            assert!(machine.step().is_ok());
        }

        assert_eq!(machine.memory[0], 8);
    }

    #[test]
    fn can_get_output() {
        let mut machine: Intcode = "1,1,1,0,4,1,99".parse().expect("Should be able to parse input");
        assert_eq!(machine.step(), Ok(None));
        assert_eq!(machine.step(), Ok(Some(1)));
        assert_eq!(machine.step(), Ok(None));

        assert_eq!(machine.state, State::Halt);
    }

    #[test]
    fn can_give_input() {
        let mut machine: Intcode = "1,1,1,0,3,0,99".parse().expect("Should be able to parse input");
        assert_eq!(machine.step(), Ok(None));
        assert_eq!(machine.step(), Ok(None));
        assert_eq!(machine.state, State::Waiting);
        assert_eq!(machine.step(), Ok(None));
        assert_eq!(machine.state, State::Waiting);

        machine.input(10);

        assert_eq!(machine.step(), Ok(None));

        assert_eq!(machine.step(), Ok(None));

        assert_eq!(machine.state, State::Halt);
        assert_eq!(machine.memory[0], 10);
    }

    #[test]
    fn compare_with_eight() {
        let mut machine: Intcode = "3,9,8,9,10,9,4,9,99,-1,8".parse().expect("Should be able to parse input");
        machine.input(5);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 0);
            }
        }

        assert_eq!(machine.state, State::Halt);

        let mut machine: Intcode = "3,3,1108,-1,8,3,4,3,99".parse().expect("Should be able to parse input");
        machine.input(8);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 1);
            }
        }

        assert_eq!(machine.state, State::Halt);
    }

    #[test]
    fn less_than_eight() {
        let mut machine: Intcode = "3,9,7,9,10,9,4,9,99,-1,8".parse().expect("Should be able to parse input");
        machine.input(5);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 1);
            }
        }

        assert_eq!(machine.state, State::Halt);

        let mut machine: Intcode = "3,3,1107,-1,8,3,4,3,99".parse().expect("Should be able to parse input");
        machine.input(50);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 0);
            }
        }

        assert_eq!(machine.state, State::Halt);
    }

    #[test]
    fn jump_testing() {
        let mut machine: Intcode = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1".parse().expect("Should be able to parse input");
        machine.input(0);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 0);
            }
        }

        assert_eq!(machine.state, State::Halt);

        let mut machine: Intcode = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9".parse().expect("Should be able to parse input");
        machine.input(3);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 1);
            }
        }

        assert_eq!(machine.state, State::Halt);

    }

    #[test]
    fn compare_complicated_with_eight() {
        let mut machine: Intcode = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".parse().expect("Should be able to parse input");
        machine.input(3);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 999);
            }
        }

        assert_eq!(machine.state, State::Halt);

        let mut machine: Intcode = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".parse().expect("Should be able to parse input");
        machine.input(8);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 1000);
            }
        }

        assert_eq!(machine.state, State::Halt);

        let mut machine: Intcode = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".parse().expect("Should be able to parse input");
        machine.input(123);
        while machine.state != State::Halt {
            if let Some(output) = machine.step().expect("Step failed") {
                assert_eq!(output, 1001);
            }
        }

        assert_eq!(machine.state, State::Halt);
    }

    #[test]
    fn run_gives_output() {
        let mut machine: Intcode = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".parse().expect("Should be able to parse input");
        let output = machine.run(123);
        assert_eq!(machine.state, State::Halt);
        assert_eq!(output, Ok(1001));
    }

    #[test]
    fn output_large_number() {
        let mut machine: Intcode = "104,1125899906842624,99".parse().expect("Should be able to parse input");
        let output = machine.run(0);
        assert_eq!(machine.state, State::Halt);
        assert_eq!(output, Ok(1_125_899_906_842_624));
    }

    #[test]
    fn output_16_digits() {
        let mut machine: Intcode = "1102,34915192,34915192,7,4,7,99,0".parse().expect("Should be able to parse input");
        let mut output = machine.run(0).expect("Should produce output");
        let mut digits = 0;
        while output > 0 {
            output = output / 10;
            digits += 1;
        }
        assert_eq!(machine.state, State::Halt);
        assert_eq!(digits, 16);

    }

    #[test]
    fn self_copy() {
        let input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
        let mut machine: Intcode = input.parse().expect("Should be able to parse input");
        let mut output = input.split(',').map(|num| i64::from_str(num).unwrap()).collect::<Vec<_>>();
        output.reverse();
        while machine.state == State::Running {
            if let Some(res) = machine.step().expect("Step failed") {
                assert_eq!(Some(res), output.pop());
            }
        }
    }
}
