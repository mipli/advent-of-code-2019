use crate::{Exercise};
use std::path::Path;
use std::fs;
use std::collections::HashSet;

/// TODO:
/// Implement the asteroid blaster as a function that returns a impl Iterator<Item=Point>, and
/// avoid recreating the asteroid lists for each asteroids we blast

pub struct Day10 {
    map: Option<Map>
}

impl Default for Day10 {
    fn default() -> Day10 {
        Day10 {
            map: None,
        }
    }
}

impl Day10 {
}

impl Exercise for Day10 {
    fn part1(&mut self, path: &Path) {
        let input = fs::read_to_string(path).expect("Could not read input file");
        let map = Map::new(&input);
        map.display();

        let spot = map
            .iter_asteroids()
            .map(|origin| {
                let count = map.evaluate_position(&origin);
                (count, origin)
            })
            .max_by(|a, b| a.0.cmp(&b.0))
            .expect("No valid asteroid found");
        println!("{:?} can see {:?} things", spot.1, spot.0);

        self.map = Some(map);

    }

    fn part2(&mut self, _path: &Path) {
        if let Some(ref map) = self.map {
            let spot = map
                .iter_asteroids()
                .map(|origin| {
                    let count = map.evaluate_position(&origin);
                    (count, origin)
                })
                .max_by(|a, b| a.0.cmp(&b.0))
                .expect("No valid asteroid found");

            let blasted = map.blast_asteroids(&spot.1);
            println!("200th asteroid victim is: {:?}", blasted[199]);
        }
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Object {
    Empty,
    Asteroid,
}

pub struct Map {
    grid: Vec<Object>,
    width: usize,
}

impl Map {
    fn new(input: &str) -> Map {
        let mut width = 0;
        let grid = input.lines().fold(vec![], |mut grid, line| {
            width = line.len();
            line.chars().for_each(|c| {
                match c {
                    '.' => grid.push(Object::Empty),
                    '#' => grid.push(Object::Asteroid),
                    _ => {}
                }
            });
            grid
        });

        Map {
            grid,
            width,
        }
    }

    fn blast_asteroids(&self, origin: &Point) -> Vec<Point> {
        let mut blasted: HashSet<Point> = HashSet::default();
        let asteroids = self.iter_asteroids().count() - 1;
        let mut output = vec![];
        let mut rotations = 0;
        let mut aim = std::f64::EPSILON;
        while blasted.len() < asteroids && rotations < 100 {
            while let Some((asteroid, angle)) = self.rotate_to_next_above(aim, origin, &blasted) {
                blasted.insert(asteroid);
                output.push(asteroid);
                aim = angle;
            }
            aim = std::f64::consts::PI/2.0 + std::f64::EPSILON;
            while let Some((asteroid, angle)) = self.rotate_to_next_below(aim, origin, &blasted) {
                blasted.insert(asteroid);
                output.push(asteroid);
                aim = angle;
            }
            rotations += 1;
            println!("Rotation #{} blasted: {}", rotations, blasted.len()); 
            aim = std::f64::consts::PI/2.0 + std::f64::EPSILON;
        }
        output
    }

    fn rotate_to_next_above(&self, aim: f64, origin: &Point, blasted: &HashSet<Point>) -> Option<(Point, f64)> {
        self.rotate_to_next(aim, origin, |point| {
            !blasted.contains(point) && point != origin && point.y() <= origin.y()
        })
    }

    fn rotate_to_next_below(&self, aim: f64, origin: &Point, blasted: &HashSet<Point>) -> Option<(Point, f64)> {
        self.rotate_to_next(aim, origin, |point| {
            !blasted.contains(point) && point != origin && point.y() > origin.y()
        })
    }

    fn rotate_to_next(&self, aim: f64, origin: &Point, filter: impl Fn(&Point) -> bool) -> Option<(Point, f64)> {
        let mut asteroids = self
            .iter_asteroids()
            .filter(|point| {
                filter(point)
            })
            .map(|p| {
                let slope = origin.slope_to(&p);
                let angle = (slope.1 as f64 / slope.0 as f64).atan();
                (p, angle)
            })
            .filter(|(_, angle)| {
                *angle < aim
            })
            .collect::<Vec<_>>();
        asteroids.sort_by(|a, b| {
            if a.1 != b.1 {
                a.1.partial_cmp(&b.1).expect("Could not compare floating angles")
            } else {
                b.0.distance(origin).partial_cmp(&a.0.distance(origin)).expect("Could not compare floating lengths")
            }
        });
        asteroids.pop()
    }

    fn evaluate_position(&self, origin: &Point) -> usize {
        let mut slopes: HashSet<(i32, i32)> = HashSet::default();
        self.iter_asteroids().for_each(|point| {
            if point != *origin {
                slopes.insert(origin.slope_to(&point));
            }
        });
        slopes.len()
    }

    fn iter_asteroids(&self) -> impl Iterator<Item=Point> + '_ {
        self.grid
            .iter()
            .enumerate()
            .filter(|(_, &o)| o == Object::Asteroid)
            .map(move |(i, _)| {
                let x = i % self.width;
                let y = i / self.width;
                Point(x as i32, y as i32)
            })
    }

    fn display(&self) {
        self.grid.chunks(self.width).for_each(|row| {
            let out = row.iter().map(|o| match o {
                Object::Empty => '.',
                Object::Asteroid => '#',
            }).collect::<String>();
            println!("{}", out);
        });
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
pub struct Point(i32, i32);

impl Point {
    #[inline]
    fn x(&self) -> i32 {
        self.0
    }

    #[inline]
    fn y(&self) -> i32 {
        self.1
    }

    fn slope_to(&self, other: &Point) -> (i32, i32) {
        let diff_x = self.x() - other.x();
        let diff_y = self.y() - other.y();

        let divisor = gcd(diff_x.abs(), diff_y.abs());
        
        if divisor == 0 {
            (0, 0)
        } else {
            (diff_y / divisor, diff_x / divisor)
        }
    }

    fn len(&self) -> f64 {
        ((self.0 * self.0) as f64 + (self.1 * self.1) as f64).sqrt()
    }

    fn distance(&self, other: &Point) -> f64 {
        Point(other.0 - self.0, other.1 - self.1).len()
    }
}

fn gcd(a: i32, b: i32) -> i32 {
    if a == 0 {
        b
    } else {
        gcd(b % a, a)
    }
}

#[cfg(test)]
mod tests {
    use super::{Map, Point};

    #[test]
    fn count_asteroids() {
        let map = Map::new(".#..#
.....
#####
....#
...##");
        assert_eq!(map.evaluate_position(&Point(3, 4)), 8);
        assert_eq!(map.evaluate_position(&Point(1, 0)), 7);
        assert_eq!(map.evaluate_position(&Point(4, 2)), 5);
    }

    #[test]
    fn blast_asteroid() {
        let map = Map::new(".#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....#...###..
..#.#.....#....##");
        let blasted = map.blast_asteroids(&Point(8, 3));
        assert_eq!(blasted.len(), 36);
        assert_eq!(blasted[0], Point(8, 1));
        assert_eq!(blasted[1], Point(9, 0));
        assert_eq!(blasted[2], Point(9, 1));
        assert_eq!(blasted[3], Point(10, 0));
        assert_eq!(blasted[4], Point(9, 2));
        assert_eq!(blasted[5], Point(11, 1));
        assert_eq!(blasted[6], Point(12, 1));
        assert_eq!(blasted[7], Point(11, 2));
        assert_eq!(blasted[8], Point(15, 1));
        assert_eq!(blasted[9], Point(12, 2));
        assert_eq!(blasted[10], Point(13, 2));
        assert_eq!(blasted[11], Point(14, 2));
        assert_eq!(blasted[12], Point(15, 2));
        assert_eq!(blasted[13], Point(12, 3));
        assert_eq!(blasted[14], Point(16, 4));
        assert_eq!(blasted[15], Point(15, 4));
    }
}
