use crate::{parse, Exercise, Intcode, State};
use std::path::Path;

pub struct Day09 {}

impl Default for Day09 {
    fn default() -> Day09 {
        Day09 {}
    }
}

impl Day09 {
    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }
}

impl Exercise for Day09 {
    fn part1(&mut self, path: &Path) {
        let mut machine: Intcode = self.get_intcode(path);
        machine.input(1);
        while machine.state == State::Running {
            if let Some(res) = machine.step().expect("Step failed") {
                println!("output: {:?}", res);
            }
        }
    }

    fn part2(&mut self, path: &Path) {
        let mut machine: Intcode = self.get_intcode(path);
        machine.input(2);
        while machine.state == State::Running {
            if let Some(res) = machine.step().expect("Step failed") {
                println!("output: {:?}", res);
            }
        }
    }
}
