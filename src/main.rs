use aoc2019::execute;
use chrono::{Datelike, Utc};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "aoc2019", about = "advent of code 2019")]
struct Opt {
    /// input
    #[structopt(parse(from_os_str))]
    input: PathBuf,

    /// day (default: today's date)
    #[structopt(short, long)]
    day: Option<u8>,

    /// skip part 1
    #[structopt(long = "no-part1")]
    no_part1: bool,
}

fn main() {
    let opt = Opt::from_args();
    execute(
        opt.day.unwrap_or_else(|| Utc::now().day() as u8),
        &opt.input,
        opt.no_part1,
    )
}
