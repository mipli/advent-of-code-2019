use crate::{parse, Exercise, Intcode, State};
use std::path::Path;

pub struct Day02 {}

impl Default for Day02 {
    fn default() -> Day02 {
        Day02 {}
    }
}

impl Day02 {
    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }
}

impl Exercise for Day02 {
    fn part1(&mut self, path: &Path) {
        let mut intcode = self.get_intcode(path);
        intcode[1] = 12;
        intcode[2] = 2;
        while intcode.state == State::Running {
            let _ = intcode.step();
        }
        println!("Part 1 output: {:?}", intcode[0]);
    }

    fn part2(&mut self, path: &Path) {
        let base_intcode = self.get_intcode(path);
        for noun in 0..=99 {
            for verb in 0..=99 {
                let mut intcode = base_intcode.clone();
                intcode[1] = noun;
                intcode[2] = verb;
                while intcode.state == State::Running {
                    let _ = intcode.step();
                }

                if intcode[0] == 19_690_720 {
                    println!("Part 2 output: {:?}", intcode[0]);
                    println!("Start: {}, {}", noun, verb);
                    return;
                }
            }
        }
    }
}
