use nom::{
  IResult,
  bytes::complete::{tag, take_till},
  combinator::map_res,
  sequence::{delimited, tuple}
};

use num_integer::Integer;
use crate::{parse, Exercise};
use std::path::Path;
use std::str::FromStr;

pub struct Day12 {
    satellites: Vec<Satellite>
}

impl Day12 {
    fn get_satellites(&self, path: &Path) -> Result<Vec<Satellite>, String> {
        parse(path).map_err(|e| format!("{:?}", e))?.try_fold(
            vec![],
            |mut sats, sat: Result<Satellite, _>| {
                sats.push(sat?);
                Ok(sats)
            },
        )
    }

    fn tick(&mut self) {
        let modifiers = self.satellites
            .iter()
            .map(|satellite| self.velocity_modifier(&satellite.position, &self.satellites.iter().map(|s| s.position).collect::<Vec<_>>()))
            .collect::<Vec<_>>();
        self.satellites
            .iter_mut()
            .zip(modifiers.iter())
            .for_each(|(sat, m)| {
                sat.velocity = sat.velocity.add(&m);
                sat.update_position();
            });
    }

    fn velocity_modifier(&self, origin: &Vector, positions: &[Vector]) -> Vector {
        positions
            .iter()
            .fold(Vector(0, 0, 0), |mut velocity, position| {
                let dx = position.x() - origin.x();
                let dy = position.y() - origin.y();
                let dz = position.z() - origin.z();
                if dx != 0 {
                    velocity.0 += dx / dx.abs();
                }
                if dy != 0 {
                    velocity.1 += dy / dy.abs();
                }
                if dz != 0 {
                    velocity.2 += dz / dz.abs();
                }
                velocity
            })
    }

    fn sum_energy(&self) -> i32 {
        self.satellites.iter().map(|s| s.get_energy()).sum()
    }

    fn get_cycle(&mut self, num: usize) -> i64 {
        let mut i: i64 = 1;
        self.tick();
        while !self.satellites.iter().all(|s| s.velocity.get(num) == 0) {
            self.tick();
            i += 1;
        }
        i * 2
    }
}

impl Default for Day12 {
    fn default() -> Day12 {
        Day12 {
            satellites: vec![]
        }
    }
}

impl Exercise for Day12 {
    fn part1(&mut self, path: &Path) {
        self.satellites = self.get_satellites(path).expect("Could not parse satellites");
        for _ in 0..1000 {
            self.tick();
        }
        println!("System energy: {:?}", self.sum_energy());
    }

    fn part2(&mut self, path: &Path) {
        let satellites = self.get_satellites(path).expect("Could not parse satellites");

        let n = (0..=2)
            .map(|n| {
                self.satellites = satellites.clone();
                self.get_cycle(n)
            })
            .fold(1, |acc, x| acc.lcm(&x));
        println!("repition: {:?}", n);
        // 307_043_147_758_488
    }
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
struct Vector(i32, i32, i32);

impl Vector {
    #[inline]
    fn x(&self) -> i32 {
        self.0
    }

    #[inline]
    fn y(&self) -> i32 {
        self.1
    }

    #[inline]
    fn z(&self) -> i32 {
        self.2
    }

    fn get(&self, i: usize) -> i32 {
        match i {
            0 => self.0,
            1 => self.1,
            2 => self.2,
            _ => panic!("invalid getting")
        }
    }

    fn add(&self, other: &Vector) -> Vector {
        Vector(self.x() + other.x(), self.y() + other.y(), self.z() + other.z())
    }

    fn magnitude(&self) -> i32 {
        self.0.abs() + self.1.abs() + self.2.abs()
    }
}



#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
struct Satellite {
    position: Vector,
    velocity: Vector
}

impl Satellite {
    fn update_position(&mut self) {
        self.position = self.position.add(&self.velocity);
    }

    fn get_energy(&self) -> i32 {
        self.position.magnitude() * self.velocity.magnitude()
    }
}

impl FromStr for Satellite {
    type Err = String;

    fn from_str(s: &str) -> Result<Satellite, Self::Err> {
        match parse_satellite(s) {
            Ok((_, satellite)) => Ok(satellite),
            Err(e) => Err(format!("{:?}", e))
        }
    }
}

fn parse_coord(input: &str) -> IResult<&str, i32> {
    let (input, _) = take_till(is_value_char)(input)?;
    let (input, value) = map_res(take_till(is_ending_char), i32::from_str)(input)?;
    Ok((input, value))
}

fn is_value_char(c: char) -> bool {
    c == '-' || c.is_digit(10)
}

fn is_ending_char(c: char) -> bool {
    !is_value_char(c)
}


fn parse_satellite(input: &str) -> IResult<&str, Satellite> {
    let (input, (x, y, z)) = delimited(tag("<"), tuple((parse_coord, parse_coord, parse_coord)), tag(">"))(input)?;

    Ok((input, Satellite {
        position: Vector(x, y, z),
        velocity: Vector(0, 0, 0)
    }))
}

#[cfg(test)]
mod tests {
    use super::{Satellite, Vector};

    #[test]
    fn parse_satellite_string() {
        let satellite: Satellite = "<x=-1, y=0, z=2>".parse().expect("Could not parse satellite string");
        assert_eq!(satellite.position, Vector(-1, 0, 2));
        assert_eq!(satellite.velocity, Vector(0, 0, 0));
    }
}
