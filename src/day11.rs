use crate::{Exercise, Intcode, parse, State};
use std::path::Path;
use std::collections::{HashMap, HashSet};

pub struct Day11 {
    map: HashMap<Point, Color>,
    painted: HashSet<Point>,
}

impl Default for Day11 {
    fn default() -> Day11 {
        Day11 {
            map: HashMap::default(),
            painted: HashSet::default(),
        }
    }
}

impl Day11 { 
    fn paint(&mut self, robot: &Robot, color: Color) {
        self.map.insert(robot.position, color);
        self.painted.insert(robot.position);
    }

    fn get_color(&self, robot: &Robot) -> Color {
        match self.map.get(&robot.position) {
            Some(color) => *color,
            None => Color::Black,
        }
    }

    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }

    fn display(&self) {
        let mut min = Point(0, 0);
        let mut max = Point(0, 0);
        self.painted.iter().for_each(|p| {
            if p.x() < min.x() {
                min.0 = p.x();
            }
            if p.y() < min.y() {
                min.1 = p.y();
            }
            if p.x() > max.x() {
                max.0 = p.x();
            }
            if p.y() > max.y() {
                max.1 = p.y();
            }
        });
        for y in min.y()..=max.y() {
            for x in min.x()..=max.x() {
                let c = match self.map.get(&Point(x, y)) {
                    Some(Color::Black) => '#',
                    _ => '.',
                };
                print!("{}", c);
            }
            println!(" ");
        }
    }
}

impl Exercise for Day11 {
    fn part1(&mut self, path: &Path) {
        let mut machine: Intcode = self.get_intcode(path);
        machine.input(0);
        let mut robot = Robot::default();
        while machine.state == State::Running {
            let paint = match machine.run_until_output().expect("Paint step should be ok") {
                Some(v) => v,
                _ => break
            };
            let face = match machine.run_until_output().expect("Face step should be ok") {
                Some(v) => v,
                _ => break
            };
            self.paint(&robot, Color::from_int(paint));
            robot.turn(face);
            robot.tick();
            machine.input(self.get_color(&robot).to_int());
        }
        println!("The robot painted {} tiles", self.painted.len());
        self.display();
    }

    fn part2(&mut self, path: &Path) {
        let mut machine: Intcode = self.get_intcode(path);
        let mut robot = Robot::default();
        self.map.clear();
        self.painted.clear();

        self.map.insert(Point(0, 0), Color::White);
        machine.input(1);

        while machine.state == State::Running {
            let paint = match machine.run_until_output().expect("Paint step should be ok") {
                Some(v) => v,
                _ => break
            };
            let face = match machine.run_until_output().expect("Face step should be ok") {
                Some(v) => v,
                _ => break
            };
            self.paint(&robot, Color::from_int(paint));
            robot.turn(face);
            robot.tick();
            machine.input(self.get_color(&robot).to_int());
        }
        println!("The robot painted {} tiles", self.painted.len());
        self.display();
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Color {
    Black,
    White
}

impl Color {
    fn to_int(self) -> i64 {
        match self {
            Color::Black => 0,
            Color::White => 1,
        }
    }
    fn from_int(value: i64) -> Color {
        match value {
            0 => Color::Black,
            1 => Color::White,
            _ => panic!("Invalid colour value: {}", value),
        }
    }
}


#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Facing {
    Up,
    Down,
    Left,
    Right
}

impl Facing {
    fn turn_left(self) -> Facing {
        use Facing::*;
        match self {
            Up => Left,
            Left => Down,
            Down => Right,
            Right => Up,
        }
    }

    fn turn_right(self) -> Facing {
        use Facing::*;
        match self {
            Up => Right,
            Left => Up,
            Down => Left,
            Right => Down,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Robot {
    facing: Facing,
    position: Point,
}

impl Default for Robot {
    fn default() -> Robot {
        Robot {
            facing: Facing::Up,
            position: Point(0, 0),
        }
    }
}

impl Robot {
    fn turn(&mut self, instruction: i64) {
        self.facing = match instruction {
            0 => self.facing.turn_left(),
            1 => self.facing.turn_right(),
            _ => panic!("Invalid turn instruction: {}", instruction),
        }
    }

    fn tick(&mut self) {
        self.position = self.position.add_facing(self.facing);
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Point(i32, i32);

impl Point {
    #[inline]
    fn x(&self) -> i32 {
        self.0
    }

    #[inline]
    fn y(&self) -> i32 {
        self.1
    }
    
    fn add_facing(&self, facing: Facing) -> Point {
        use Facing::*;

        match facing {
            Up => Point(self.x(), self.y() - 1),
            Down => Point(self.x(), self.y() + 1),
            Left => Point(self.x() - 1, self.y()),
            Right => Point(self.x() + 1, self.y()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Robot, Facing, Point, Color, Day11};

    #[test]
    fn basic_operations() {
        let mut robot = Robot::default();
        robot.turn(0);
        robot.tick();
        assert_eq!(robot.facing, Facing::Left);
        assert_eq!(robot.position, Point(-1, 0));
    }

    #[test]
    fn basic_painting() {
        let mut world = Day11::default();
        let mut robot = Robot::default();
        let ints = [(1, 0), (0, 0), (1, 0), (1, 0), (0, 1), (1, 0), (1, 0)];
        for &(colour, instruction) in &ints {
            world.paint(&robot, Color::from_int(colour as i64));
            robot.turn(instruction as i64);
            robot.tick();
        }
        assert_eq!(robot.facing, Facing::Left);
        assert_eq!(robot.position, Point(0, -1));
        assert_eq!(world.map.iter().map(|(_, v)| v).filter(|&&c| c == Color::White).count(), 4);
        assert_eq!(world.painted.len(), 6);
    }
}
