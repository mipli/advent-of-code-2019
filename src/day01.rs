use crate::{parse, Exercise};
use std::path::Path;

pub struct Day01 {}

impl Default for Day01 {
    fn default() -> Day01 {
        Day01 {}
    }
}

impl Day01 {
    fn get_masses(&self, path: &Path) -> Result<Vec<i32>, String> {
        parse(path).map_err(|e| format!("{:?}", e))?.try_fold(
            vec![],
            |mut masses, mass: Result<i32, _>| {
                masses.push(mass?);
                Ok(masses)
            },
        )
    }
}

impl Exercise for Day01 {
    fn part1(&mut self, path: &Path) {
        match self.get_masses(path) {
            Ok(masses) => {
                let req = masses.into_iter().map(fuel_per_mass).sum::<i32>();
                println!("Fuel requirements: {:?}", req);
            }
            Err(s) => eprintln!("Error: {:?}", s),
        }
    }

    fn part2(&mut self, path: &Path) {
        match self.get_masses(path) {
            Ok(masses) => {
                let req = masses.into_iter().fold(0i32, |acc, mass| {
                    let mut new = fuel_per_mass(mass);
                    let mut extra = fuel_per_mass(new);
                    while extra > 0 {
                        new += extra;
                        extra = fuel_per_mass(extra);
                    }
                    acc + new
                });
                println!("Fuel requirements, all inclusive: {}", req);
            }
            Err(s) => eprintln!("Error: {:?}", s),
        }
    }
}

fn fuel_per_mass(mass: i32) -> i32 {
    mass / 3 - 2
}
