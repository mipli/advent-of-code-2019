use crate::{parse, Exercise, Intcode};
use std::path::Path;

pub struct Day05 {}

impl Default for Day05 {
    fn default() -> Day05 {
        Day05 {}
    }
}

impl Day05 {
    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }
}

impl Exercise for Day05 {
    fn part1(&mut self, path: &Path) {
        let mut machine = self.get_intcode(path);
        let response = machine.run(1).expect("Crash!");
        println!("Part 1 output: {:?}", response);
    }

    fn part2(&mut self, path: &Path) {
        let mut machine = self.get_intcode(path);
        let response = machine.run(5).expect("Crash!");
        println!("Part 2 output: {:?}", response);
    }
}
