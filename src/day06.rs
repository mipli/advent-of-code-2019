use crate::{parse, Exercise};
use std::str::FromStr;
use std::path::Path;
use std::collections::{HashMap};

pub struct Day06 {
    system: System
}

impl Default for Day06 {
    fn default() -> Day06 {
        Day06 {
            system: System::default()
        }
    }
}

impl Day06 {
    fn get_orbits(&self, path: &Path) -> Result<Vec<Orbit>, String> {
        parse(path).map_err(|e| format!("{:?}", e))?.try_fold(
            vec![],
            |mut orbits, orbit: Result<Orbit, _>| {
                orbits.push(orbit?);
                Ok(orbits)
            },
        )
    }
}

impl Exercise for Day06 {
    fn part1(&mut self, path: &Path) {
        let orbits = self.get_orbits(path).expect("Could not parse orbit input");
        self.system = System::default();
        orbits.iter().for_each(|orbit| {
            self.system.add_orbit(orbit);
        });
        println!("total orbits: {:?}", self.system.total_orbits());
    }

    fn part2(&mut self, _path: &Path) {
        let path = self.system.transfer("YOU", "SAN").expect("No path found");
        println!("transfer length: {:?}", path.len() - 1);
    }
}

struct Orbit {
    object: String,
    source: String,
}

impl FromStr for Orbit {
    type Err = String;

    fn from_str(s: &str) -> Result<Orbit, Self::Err> {
        match &s.split(')').collect::<Vec<&str>>()[..] {
            &[source, object] => Ok(Orbit {
                source: source.to_string(),
                object: object.to_string(),
            }),
            _ => {
                Err(format!("Invalid orbit definition: {}", s).to_string())
            }
        }
    }
}

struct System {
    orbits: HashMap<String, Vec<String>>
}

impl System {
    fn add_orbit(&mut self, orbit: &Orbit) {
        self.orbits.entry(orbit.object.to_string()).or_default().push(orbit.source.to_string());
    }

    fn path_to_origin(&self, object: &str) -> Option<Vec<String>> {
        if object == "COM" {
            return Some(vec![]);
        }

        let orbits = self.orbits.get(object)?;
        let paths = orbits.iter().map(|o| {
            let mut path = self.path_to_origin(o).expect("All object should have path to COM");
            path.push(o.to_string());
            path
        });
        let min_path = paths.into_iter().min_by(|a: &Vec<_>, b: &Vec<_>| {
            a.len().cmp(&b.len())
        });
        return min_path;
    }

    fn transfer(&self, source: &str, target: &str) -> Option<Vec<String>> {
        let mut source_path = self.path_to_origin(source)?;
        let mut target_path = self.path_to_origin(target)?;
        source_path.reverse();
        target_path.reverse();
        let source_index = source_path.iter().position(|p| target_path.contains(p))?;
        let target_index = target_path.iter().position(|p| p == &source_path[source_index])?;

        let mut path = source_path[0..=source_index].to_vec();
        path.append(&mut target_path[0..target_index].to_vec());

        Some(path)
    }

    fn count_orbits(&self, object: &str) -> usize {
        if let Some(orbits) = self.orbits.get(object) {
            orbits
                .iter()
                .map(|orbit| 1 + self.count_orbits(orbit))
                .sum()
        } else {
            0
        }
    }

    fn total_orbits(&self) -> usize {
        self.orbits.keys().map(|object| self.count_orbits(object)).sum()
    }

}

impl Default for System {
    fn default() -> Self {
        System {
            orbits: HashMap::default()
        }
    }
}
