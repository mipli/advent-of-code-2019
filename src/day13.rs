use crate::{parse, Exercise, Intcode, State};
use std::path::Path;
use std::collections::HashMap;

pub struct Day13 {}

impl Default for Day13 {
    fn default() -> Day13 {
        Day13 {}
    }
}

impl Day13 {
    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }

    fn get_draw_code(&self, machine: &mut Intcode) -> Option<DrawResult> {
        let x = machine.run_until_output().unwrap()?;
        let y = machine.run_until_output().unwrap()?;
        let v = machine.run_until_output().unwrap()?;
        if x == -1 && y == 0 {
            Some(DrawResult::Score(v))
        } else {
            Some(DrawResult::Tile(Point(x, y), Tile::from(v)))
        }
    }
}

enum DrawResult {
    Tile(Point, Tile),
    Score(i64)
}

impl Exercise for Day13 {
    fn part1(&mut self, path: &Path) {
        let mut machine = self.get_intcode(path);
        let mut grid: HashMap<Point, Tile> = HashMap::default();
        while machine.state != State::Halt {
            if let Some(DrawResult::Tile(point, tile)) = self.get_draw_code(&mut machine) {
                grid.insert(point, tile);
            }
        }
        let blocks = grid
            .iter()
            .filter(|(_, &v)| v == Tile::Block)
            .count();
        println!("Block count: {}", blocks);
    }

    fn part2(&mut self, path: &Path) {
        let mut machine = self.get_intcode(path);
        let mut paddle = Point(0, 0);

        machine[0] = 2;
        let mut score = 0;
        while machine.state != State::Halt {
            match self.get_draw_code(&mut machine) {
                Some(DrawResult::Tile(point, tile)) => {
                    use std::cmp::Ordering::*;
                    match tile {
                        Tile::Paddle => paddle = point,
                        Tile::Ball => match paddle.0.cmp(&point.0) {
                            Greater => machine.input(-1),
                            Less => machine.input(1),
                            Equal => machine.input(0),
                        },
                        _ => {}
                    }
                },
                Some(DrawResult::Score(v)) => score = v,
                None => {}
            }
        }
        println!("Score: {}", score);
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Tile {
    Empty(i64),
    Wall,
    Block,
    Paddle,
    Ball
}


impl From<i64> for Tile {
    fn from(value: i64) -> Tile {
        match value {
            1 => Tile::Wall,
            2 => Tile::Block,
            3 => Tile::Paddle,
            4 => Tile::Ball,
            _ => Tile::Empty(value),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
struct Point(i64, i64);
