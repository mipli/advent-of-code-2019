use crate::{parse, Exercise, Intcode, State};
use std::path::Path;
use itertools::Itertools;

pub struct Day07 {}

impl Default for Day07 {
    fn default() -> Day07 {
        Day07 {}
    }
}

impl Day07 {
    fn get_intcode(&self, path: &Path) -> Intcode {
        if let Some(res) = parse(path).expect("Could not open file").next() {
            res.expect("Could not parse Intcode")
        } else {
            panic!("Error parsing Intcode from input file");
        }
    }

    fn run_amplifier(&self, machine: &mut Intcode, phase: i64, input: i64) -> i64 {
        let _ = machine.run(phase).expect("Crash!");
        machine.run(input).expect("Crash!")
    }

    fn get_thruster_output(&self, machine: &Intcode, phases: &[i64]) -> i64 {
        let a = self.run_amplifier(&mut machine.clone(), phases[0], 0);
        let b = self.run_amplifier(&mut machine.clone(), phases[1], a);
        let c = self.run_amplifier(&mut machine.clone(), phases[2], b);
        let d = self.run_amplifier(&mut machine.clone(), phases[3], c);
        self.run_amplifier(&mut machine.clone(), phases[4], d)
    }

    fn get_feedback_output(&self, machine: &Intcode, phases: &[i64]) -> i64 {
        let mut amp_a = machine.clone();
        let mut amp_b = machine.clone();
        let mut amp_c = machine.clone();
        let mut amp_d = machine.clone();
        let mut amp_e = machine.clone();
        let mut a = self.run_amplifier(&mut amp_a, phases[0], 0);
        let mut b = self.run_amplifier(&mut amp_b, phases[1], a);
        let mut c = self.run_amplifier(&mut amp_c, phases[2], b);
        let mut d = self.run_amplifier(&mut amp_d, phases[3], c);
        let mut e = self.run_amplifier(&mut amp_e, phases[4], d);
        while amp_e.state != State::Halt {
            a = amp_a.run(e).expect("Crash!");
            b = amp_b.run(a).expect("Crash!");
            c = amp_c.run(b).expect("Crash!");
            d = amp_d.run(c).expect("Crash!");
            e = amp_e.run(d).expect("Crash!");
        }
        e
    }

    fn get_max_thrust(&self, machine: &Intcode) -> i64 {
        (0..5).permutations(5).map(|permutation| {
            self.get_thruster_output(machine, &permutation)
        }).max().expect("No phase combinations worked!")
    }

    fn get_feedback_thrust(&self, machine: &Intcode) -> i64 {
        (5..10).permutations(5).map(|permutation| {
            self.get_feedback_output(machine, &permutation)
        }).max().expect("No phase combinations worked!")
    }
}

impl Exercise for Day07 {
    fn part1(&mut self, path: &Path) {
        let machine: Intcode = self.get_intcode(path);
        let max = self.get_max_thrust(&machine);
        println!("Part 1 max: {:?}", max);
    }

    fn part2(&mut self, path: &Path) {
        let machine: Intcode = self.get_intcode(path);
        let max = self.get_feedback_thrust(&machine);
        println!("Part 1 max: {:?}", max);
    }
}
